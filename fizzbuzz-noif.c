/* for asprintf */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

char *strs[] = {
	0,
	"fizz",
	"buzz",
	"fizzbuzz"
};

int ind[] = {0, 0, 1, 0, 2, 1, 0, 0, 1, 2, 0, 1, 0, 0, 3};
int ind_len = sizeof(ind) / sizeof(ind[0]);

int main(void) {
	for(int i = 1; i < 101; i++) {
		asprintf(&(strs[0]), "%d", i);
		printf("%s\n", strs[ind[(i-1) % ind_len]]);
		free(strs[0]);
	}

	return 0;
}

